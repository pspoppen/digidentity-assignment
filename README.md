# README

To reproduce this project:

1. Clone git repository: 
2. Run bundle install
3. Run Rails db:create
4. Run Rails db:migrate
5. Run run Rails console

## Assignment 1 (Calculate 0/1):

1. Target project folder and open IRB session

2. Open a file    
   `file = File.binread(Rails.root.join("app", "assets", "images", "example.jpg"))`

3. Calculate bits    
	`file.calculate_bits`
	
	Gives result:  
   "the file has 160952 zero's!"   
   "the file has 101416 one's!"
   
## Assignment 2 (Mini banking app):

1. Create user and target balance variable  
   `hans= User.create(email: 'hans@bank.nl', password: '123456')`   
   `balance_hans = hans.user_balance`

2. Add credit to user_balance  
   `balance_hans.add_credit(100.00)`
   
3. Check current_user_balance  
   `balance_hans.current_balance`
   
4. Create second user and target balance variable  
   `marieke= User.create(email: 'marieke@bank.nl', password: '123456')`\
   `balance_marieke = marieke.user_balance`

5. Add credit for second user    
   `balance_marieke.add_credit(325,00)`\
   `balance_marieke.new_transaction('hans@bank.nl', 75.00)`\
   
6. check resulting balances  
   `balance_hans.current_balance` (result = 175)   
   `balance_marieke.current_balance` (result = 250) 
   
**(Optional with user_login):**   
1. Start rails server (rails s)  
2. Login with first user email and password.   
3. Devise will authenticate user and run into the PRY in the transactions_controller  
4. Repeat steps 1-6 and access user via current_user  
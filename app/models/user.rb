class User < ApplicationRecord
	# Include default devise modules. Others available are:
	# :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
	devise :database_authenticatable, :registerable,
	       :recoverable, :rememberable, :validatable

	has_one :user_balance, dependent: :destroy

	after_create :create_user_balance

	def create_user_balance
		UserBalance.create(
			account_name: "Betaalrekening #{self.email}",
			bank_name: "JGZ bank",
			user: self
		)
	end
end

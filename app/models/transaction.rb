class Transaction < ApplicationRecord
	belongs_to :receiving_balance, class_name: 'UserBalance'
	belongs_to :sending_balance, class_name: 'UserBalance', optional: true

	scope :pending, lambda {
		where(succeeded: false)
	}

	scope :succeeded, lambda {
		where(succeeded: true)
	}

	enum transaction_type: TRANSACTION_TYPES
	enum on_hold_type: ON_HOLD_TYPES
end
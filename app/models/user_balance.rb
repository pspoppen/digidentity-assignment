class UserBalance < ApplicationRecord
	belongs_to :user
	has_many :sent_transactions, foreign_key: :sending_balance_id, class_name: 'Transaction'
	has_many :received_transactions, foreign_key: :receiving_balance_id, class_name: 'Transaction'

	def add_credit(amount)
		Transaction.create(
			transaction_type: 0,
			amount: amount,
			succeeded: true,
			receiving_balance: self
		)
	end

	def current_balance
		self.received_transactions.succeeded.pluck(:amount).sum - self.sent_transactions.succeeded.pluck(:amount).sum
	end

	def new_transaction(receiving_user_email, amount)
		receiving_user_balance = User.find_by(email: receiving_user_email).user_balance

		if self.current_balance >= amount
			Transaction.create(
				transaction_type: 1,
				amount: amount,
				succeeded: false,
				sending_balance: self,
				receiving_balance: receiving_user_balance,
				on_hold_type: 0
			)
		else
			Transaction.create(
				transaction_type: 1,
				amount: amount,
				succeeded: true,
				sending_balance: self,
				receiving_balance: receiving_user_balance
			)
		end
	end
end
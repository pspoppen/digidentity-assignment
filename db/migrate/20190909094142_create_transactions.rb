class CreateTransactions < ActiveRecord::Migration[6.0]
	def change
		create_table :transactions do |t|
			t.references :sending_balance, index: true, foreign_key: { to_table: :user_balances }
			t.references :receiving_balance, index: true, foreign_key: { to_table: :user_balances }
			t.float :amount
			t.integer :transaction_type
			t.integer :on_hold_type
			t.boolean :succeeded
		end
	end
end

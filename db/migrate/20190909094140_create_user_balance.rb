class CreateUserBalance < ActiveRecord::Migration[6.0]
	def change
		create_table :user_balances do |t|
			t.string :account_name
			t.string :bank_name
			t.references :user, foreign_key: false
		end

		add_foreign_key :user_balances, :users, column: :user_id
	end
end

# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_09_09_094142) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "transactions", force: :cascade do |t|
    t.bigint "sending_balance_id"
    t.bigint "receiving_balance_id"
    t.float "amount"
    t.integer "transaction_type"
    t.integer "on_hold_type"
    t.boolean "succeeded"
    t.index ["receiving_balance_id"], name: "index_transactions_on_receiving_balance_id"
    t.index ["sending_balance_id"], name: "index_transactions_on_sending_balance_id"
  end

  create_table "user_balances", force: :cascade do |t|
    t.string "account_name"
    t.string "bank_name"
    t.bigint "user_id"
    t.index ["user_id"], name: "index_user_balances_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "transactions", "user_balances", column: "receiving_balance_id"
  add_foreign_key "transactions", "user_balances", column: "sending_balance_id"
  add_foreign_key "user_balances", "users"
end
